from django.apps import AppConfig


class StorykeduaConfig(AppConfig):
    name = 'storykedua'
