from django.urls import path
from . import views

app_name = 'storykedua'
 
urlpatterns = [
    path('', views.story1, name='storykedua')
]