from django.shortcuts import render

# Create your views here.

def welcome(request):
    return render(request, 'storykeempat/welcome.html')

def profile(request):
    return render(request, 'storykeempat/profile.html')

def experience(request):
    return render(request, 'storykeempat/experience.html')

