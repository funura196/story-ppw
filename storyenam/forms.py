from django import forms

class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
        label="Nama kegiatan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60',
            }
        )
    )

    deskripsi = forms.CharField(
        label="Deskripsi kegiatan",
        max_length=1000,
        widget=forms.Textarea(
            
        )
    )

class FormDaftar(forms.Form):
    nama = forms.CharField(
        label="Your Name",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )
 
